import os

import django

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'distributionproject.settings')
django.setup()

import requests
from requests.exceptions import RequestException
from django.db.models import ObjectDoesNotExist
from django.utils import timezone
from celery import Celery
from celery.schedules import crontab

from mainapp.models import *
from mainapp.utils import dispatch_request_to_api

app = Celery('distributionproject')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()


@app.on_after_configure.connect
def setup_periodic_tasks(sender, **kwargs):
    sender.add_periodic_task(crontab(), periodic_check_messages_distribution.s())


@app.task
def periodic_check_messages_distribution():
    for message in Message.objects.select_related().filter(distribution__started_at__lte=timezone.now(),
                                                           distribution__finished_at__gt=timezone.now(),
                                                           status__in=[Message.NOT_STARTED, Message.PENDING]):
        data = {"id": message.pk, "phone": int(message.client.phone), "text": message.distribution.message_text}
        resp = dispatch_request_to_api(message_id=message.pk, data=data)
        print(f'Api call for message {message.pk} has status code {resp.status_code} in periodic task')

        if resp.status_code == 200:
            message.status = Message.SUCCEEDED
            message.save(update_fields=['status'])


@app.task(autoretry_for=(RequestException,), retry_kwargs={'max_retries': 5}, default_retry_delay=60)
def dispatch_message_to_api(message_id: int):
    try:
        message = Message.objects.select_related().get(pk=message_id)
    except ObjectDoesNotExist as e:
        print(f"Message with id={message_id} doesn't exist!")
    else:
        message.status = Message.PENDING
        message.save(update_fields=['status'])

        data = {"id": message.pk, "phone": int(message.client.phone), "text": message.distribution.message_text}
        resp = dispatch_request_to_api(message_id=message.pk, data=data)
        print(f'Api call for message {message.pk} has status code {resp.status_code}')

        if resp.status_code == 200:
            message.status = Message.SUCCEEDED
            message.save(update_fields=['status'])
            return True

        return False
