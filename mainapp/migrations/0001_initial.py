# Generated by Django 3.1 on 2022-03-10 20:32

from django.conf import settings
import django.core.validators
from django.db import migrations, models
import django.db.models.deletion
import mainapp.models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('auth', '0012_alter_user_first_name_max_length'),
    ]

    operations = [
        migrations.CreateModel(
            name='Client',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(blank=True, null=True, verbose_name='last login')),
                ('is_superuser', models.BooleanField(default=False, help_text='Designates that this user has all permissions without explicitly assigning them.', verbose_name='superuser status')),
                ('phone', models.CharField(db_index=True, max_length=11, unique=True, validators=[django.core.validators.RegexValidator(message='Допустимы только числовые значения!', regex='7\\d{10}')], verbose_name='Номер телефона')),
                ('tag', models.TextField(verbose_name='Произвольная метка')),
                ('time_zone', models.CharField(default='UTC', max_length=30, verbose_name='Часовой пояс')),
                ('is_staff', models.BooleanField(default=False, verbose_name='Может ли заходить в админку')),
                ('groups', models.ManyToManyField(blank=True, help_text='The groups this user belongs to. A user will get all permissions granted to each of their groups.', related_name='user_set', related_query_name='user', to='auth.Group', verbose_name='groups')),
                ('user_permissions', models.ManyToManyField(blank=True, help_text='Specific permissions for this user.', related_name='user_set', related_query_name='user', to='auth.Permission', verbose_name='user permissions')),
            ],
            options={
                'verbose_name': 'Клиент',
                'verbose_name_plural': 'Клиенты',
            },
            managers=[
                ('objects', mainapp.models.ClientManager()),
            ],
        ),
        migrations.CreateModel(
            name='Distribution',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('started_at', models.DateTimeField(blank=True, null=True, verbose_name='Дата и время запуска рассылки')),
                ('finished_at', models.DateTimeField(blank=True, null=True, verbose_name='Дата и время окончания рассылки')),
                ('message_text', models.TextField(verbose_name='Текст сообщения для доставки клиенту')),
                ('property_filter', models.CharField(blank=True, max_length=256, null=True, verbose_name='Фильтр свойств для клиентов')),
            ],
            options={
                'verbose_name': 'Рассылка',
                'verbose_name_plural': 'Рассылки',
            },
        ),
        migrations.CreateModel(
            name='Message',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Дата и время создания сообщения')),
                ('status', models.CharField(choices=[('pending', 'Pending'), ('succeeded', 'Succeeded'), ('not_started', 'Not_started')], default='not_started', max_length=100)),
                ('client', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
                ('distribution', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='mainapp.distribution')),
            ],
            options={
                'verbose_name': 'Сообщение',
                'verbose_name_plural': 'Сообщения',
            },
        ),
    ]
