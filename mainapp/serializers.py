from rest_framework.serializers import ModelSerializer, Serializer, IntegerField
from .models import *


class ClientSerializer(ModelSerializer):
    class Meta:
        model = Client
        fields = ['id', 'phone', 'tag', 'time_zone', 'operator_code']


class DistributionSerializer(ModelSerializer):
    class Meta:
        model = Distribution
        fields = ['id', 'started_at', 'finished_at', 'message_text', 'property_filter']


class DistributionStatsSerializer(Serializer):
    distribution = DistributionSerializer()
    not_started = IntegerField()
    on_pending = IntegerField()
    succeeded = IntegerField()

    class Meta:
        fields = ['distribution', 'not_started', 'on_pending', 'succeeded']
