import json

import requests
from django.conf import settings


def dispatch_request_to_api(message_id: int, data: dict):
    token = settings.PROBE_TOKEN
    connect_timeout, read_timeout = 5.0, 30.0
    resp = requests.post(f"https://probe.fbrq.cloud/v1/send/{message_id}",
                         headers={'Content-Type': 'application/json', 'Accept': 'application/json',
                                  'Authorization': f'Bearer {token}'},
                         data=json.dumps(data),
                         timeout=(connect_timeout, read_timeout))
    return resp
