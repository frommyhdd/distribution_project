import datetime

from django.test import TestCase
from django.utils import timezone as dj_timezone
from rest_framework import status
from rest_framework.test import APIRequestFactory, force_authenticate

from mainapp.models import *
from mainapp.views import DistributionViewSet, ClientViewSet


class DistributionTest(TestCase):

    def setUp(self):
        self.factory = APIRequestFactory()
        self.client = Client.objects.create(phone="79531234567", password='testpass1', tag='Ivan')
        self.admin = Client.objects.create_superuser(phone='79531234568', password='?Adminpass12345', tag='Petr')

    def test_post_distributions(self):
        date_from = dj_timezone.now() - datetime.timedelta(minutes=10)
        date_to = dj_timezone.now() + datetime.timedelta(days=1)

        request = self.factory.get('/api/client/')
        force_authenticate(request, user=self.admin)
        response = ClientViewSet.as_view({'get': 'list'})(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        request = self.factory.post(f'/api/distribution/',  data={"started_at": date_from,
                                                                  "finished_at": date_to,
                                                                  "message_text": "Hello12345",
                                                                  "property_filter": "953"})
        force_authenticate(request, user=self.admin)
        response = DistributionViewSet.as_view({'post': 'create'})(request)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
