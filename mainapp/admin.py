from django.contrib import admin
from django.contrib.auth.models import Permission
from .models import *

admin.site.register(Permission)
admin.site.register(Client)
admin.site.register(Distribution)
admin.site.register(Message)
