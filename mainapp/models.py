from django.db import models, transaction
from django.core.validators import RegexValidator
from django.contrib.auth.base_user import BaseUserManager
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from django.db.models import Q, F, signals, Count
from django.dispatch import receiver
from django.utils import timezone


class ClientManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, phone, password, **extra_fields):
        if not phone:
            raise ValueError('The given phone must be set')
        user = self.model(phone=phone, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, phone, password=None, **extra_fields):
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(phone, password, **extra_fields)

    def create_superuser(self, phone, password, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(phone, password, **extra_fields)


class Client(AbstractBaseUser, PermissionsMixin):
    USERNAME_FIELD = 'phone'

    phone = models.CharField(max_length=11, unique=True, verbose_name='Номер телефона',
                             validators=[RegexValidator(regex='7\d{10}', message='Допустимы только числовые значения!')])
    tag = models.TextField(verbose_name='Произвольная метка')
    time_zone = models.CharField(max_length=30, default='UTC', verbose_name='Часовой пояс')
    operator_code = models.CharField(max_length=3, verbose_name='Код мобильного оператора', db_index=True, null=True, blank=True)

    is_staff = models.BooleanField(verbose_name='Может ли заходить в админку', default=False)
    objects = ClientManager()

    def get_username(self):
        return self.phone

    class Meta:
        verbose_name = 'Клиент'
        verbose_name_plural = 'Клиенты'

    def __str__(self):
        return f'{self.pk} | {self.phone}'


class Distribution(models.Model):
    started_at = models.DateTimeField(null=True, blank=True, verbose_name='Дата и время запуска рассылки')
    finished_at = models.DateTimeField(null=True, blank=True, verbose_name='Дата и время окончания рассылки')
    message_text = models.TextField(verbose_name='Текст сообщения для доставки клиенту')
    property_filter = models.CharField(max_length=256, null=True, blank=True, verbose_name='Фильтр свойств для клиентов')

    class Meta:
        verbose_name = 'Рассылка'
        verbose_name_plural = 'Рассылки'

    def __str__(self):
        return f'{self.pk}'

    def create_messages_for_distribution(self):
        for client in Client.objects.filter(Q(tag__icontains=self.property_filter) | Q(operator_code=self.property_filter)):
            m = Message.objects.create(client=client, distribution=self)
            print(f"message {m.pk} successfull created")

    def get_status_groups(self):
        not_started, pending, succeeded = 0, 0, 0
        for element in self.message_set.values('status').annotate(total=Count('status')):
            if element.get('status', None) == 'not_started':
                not_started = element.get('total', 0)
            if element.get('status', None) == 'pending':
                pending = element.get('total', 0)
            if element.get('status', None) == 'succeeded':
                succeeded = element.get('total', 0)
        return not_started, pending, succeeded


class Message(models.Model):
    PENDING = 'pending'
    SUCCEEDED = 'succeeded'
    NOT_STARTED = 'not_started'

    STATUS_CHOICES = (
        (PENDING, 'Pending'),
        (SUCCEEDED, 'Succeeded'),
        (NOT_STARTED, 'Not_started'),
    )

    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Дата и время создания сообщения')
    status = models.CharField(max_length=100, choices=STATUS_CHOICES, default=NOT_STARTED)
    distribution = models.ForeignKey(Distribution, on_delete=models.CASCADE)
    client = models.ForeignKey(Client, on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Сообщение'
        verbose_name_plural = 'Сообщения'

    def __str__(self):
        return f'{self.pk} | {self.created_at} | {self.status}'


def __message_post_save(sender, instance, created, **kwargs):
    if created:
        from distributionproject.celery import dispatch_message_to_api
        if instance.distribution.started_at <= timezone.now() < instance.distribution.finished_at:
            res = dispatch_message_to_api.delay(instance.pk)
            result = res.get()
            information = f'Successfull sending message - {instance.pk} to API' if result else 'API didn\'t get the message'
            print(information)


@receiver(signal=signals.post_save, sender=Message)
def message_post_save(sender, instance, created, **kwargs):
    transaction.on_commit(lambda: __message_post_save(sender, instance, created, **kwargs))


def __client_post_save(sender, instance, created, **kwargs):
    if created:
        instance.operator_code = instance.phone[1:4]
        instance.save(update_fields=['operator_code'])


@receiver(signal=signals.post_save, sender=Client)
def client_post_save(sender, instance, created, **kwargs):
    transaction.on_commit(lambda: __client_post_save(sender, instance, created, **kwargs))