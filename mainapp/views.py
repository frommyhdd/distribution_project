from django.utils import timezone
from django.db.models import Count, Q
from rest_framework import permissions, viewsets, status
from rest_framework.response import Response
from rest_framework.decorators import action
from drf_yasg.inspectors import SwaggerAutoSchema
from drf_yasg.utils import swagger_auto_schema

from .serializers import *
from .models import *


class ClientViewSet(viewsets.ModelViewSet):
    swagger_schema = SwaggerAutoSchema
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = ClientSerializer
    queryset = Client.objects.all()


class DistributionViewSet(viewsets.ModelViewSet):
    swagger_schema = SwaggerAutoSchema
    permission_classes = (permissions.IsAuthenticated,)
    queryset = Distribution.objects.all()

    def get_serializer_class(self):
        if self.action in ['get_stats_by_single_distribution', 'get_stats_by_all_distributions']:
            return DistributionStatsSerializer
        else:
            return DistributionSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        distribution = serializer.save()

        if distribution.finished_at <= timezone.now():
            return Response({'description': 'Distribution finished time has expired'},
                            status=status.HTTP_400_BAD_REQUEST)

        distribution.create_messages_for_distribution()

        return Response(serializer.data, status=status.HTTP_201_CREATED)

    @swagger_auto_schema(operation_description="Get statistics by single distribution")
    @action(detail=True)
    def get_stats_by_single_distribution(self, request, pk=None):
        distribution = self.get_object()
        not_started, pending, succeeded = distribution.get_status_groups()
        data = {'distribution': distribution, 'not_started': not_started, 'on_pending': pending, 'succeeded': succeeded}
        serializer = self.get_serializer(data)
        return Response(serializer.data, status=status.HTTP_200_OK)

    @swagger_auto_schema(operation_description="Get statistics by all distributions")
    @action(detail=False)
    def get_stats_by_all_distributions(self, request):
        data = []
        for distribution in Distribution.objects.all():
            not_started, pending, succeeded = distribution.get_status_groups()
            entity = {'distribution': distribution, 'not_started': not_started, 'on_pending': pending, 'succeeded': succeeded}
            data.append(entity)
        serializer = self.get_serializer(data, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)
